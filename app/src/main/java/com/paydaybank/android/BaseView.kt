package com.paydaybank.android

interface BaseView {

    fun showProgress()

    fun hideProgress()

    fun showErrorMessage(message: String)

    fun showDataView()

    fun hideDataView()

    fun showEmptyView()

    fun hideEmptyView()

}