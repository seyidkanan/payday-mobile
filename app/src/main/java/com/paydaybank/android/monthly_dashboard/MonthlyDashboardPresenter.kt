package com.paydaybank.android.monthly_dashboard

import com.paydaybank.android.api.TransactionInteractor
import com.paydaybank.android.pojo.TransactionData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class MonthlyDashboardPresenter(private var view: MonthlyDashboardView) {

    private var startDate: String? = null
    private var endDate: String? = null

    private var transactionInteractor: TransactionInteractor = TransactionInteractor()

    constructor(view: MonthlyDashboardView, transactionInteractor: TransactionInteractor) : this(
        view
    ) {
        this.transactionInteractor = transactionInteractor
        this.view = view
    }

    fun makeMonthlyReport() {
        view.clearData()
        if (startDate != null && endDate != null) {
            val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            val firstDate = sdf.parse(startDate)
            val secondDate = sdf.parse(endDate)

            val diffInMillies = Math.abs(secondDate.time - firstDate.time)
            val diff: Long = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)

            if (diff > 90) {
                view.showErrorMessage("error more than 3 months")
            } else {
                view.showProgress()
                transactionInteractor.fetchTransaction(
                    accountId = 1,
                    object : Callback<MutableList<TransactionData>> {
                        override fun onResponse(
                            call: Call<MutableList<TransactionData>>,
                            response: Response<MutableList<TransactionData>>
                        ) {
                            view.hideProgress()
                            if (response.isSuccessful) {
                                response.body()?.let {
                                    if (it.size > 0) {
                                        makeReport(startDate, endDate, it)
                                    } else {
                                        view.showEmptyView()
                                        view.hideDataView()
                                    }
                                }
                            }
                        }

                        override fun onFailure(
                            call: Call<MutableList<TransactionData>>,
                            t: Throwable
                        ) {
                            view.hideProgress()
                            t.message?.let { view.showErrorMessage(it) }
                            view.showEmptyView()
                            view.hideDataView()
                        }
                    })
            }
        }
    }

    fun makeReport(
        startDate: String?,
        endDate: String?,
        transactionList: MutableList<TransactionData>
    ) {

        val sdf = SimpleDateFormat("yyyy-MM-dd")
        var startDateLocal: Date? = null
        var endDateLocal: Date? = null
        try {
            startDateLocal = sdf.parse(startDate)
            endDateLocal = sdf.parse(endDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val sortedList = mutableListOf<TransactionData>()

        transactionList.forEach {
            val transactionDate = sdf.parse(it.date)
            if (startDateLocal?.before(transactionDate)!!) {
                if (endDateLocal?.after(transactionDate)!!) {
                    sortedList.add(it)
                }
            }
        }

        makeReportAllEarningAndSpending(sortedList)
        makeReportForCategory(sortedList)
    }

    private fun makeReportForCategory(sortedList: MutableList<TransactionData>) {
        val categoryMapForMonth = mutableMapOf<String, MutableList<TransactionData>>()
        val categoryMapForMonthFinal = mutableMapOf<String, MutableMap<String, Double>>()

        sortedList.forEach {
            if (categoryMapForMonth.keys.contains(it.date?.substring(0, 7))) {
                val list = categoryMapForMonth[it.date?.substring(0, 7)]
                list?.add(it)
                categoryMapForMonth[it.date?.substring(0, 7)!!] = list!!
            } else {
                val mutableList = mutableListOf<TransactionData>()
                mutableList.add(it)
                categoryMapForMonth.put(
                    it.date?.substring(0, 7)!!,
                    mutableList
                )
            }
        }

        categoryMapForMonth.iterator().forEach {
            val transactionDataList = it.value
            val mapCategory = mutableMapOf<String, Double>()
            for (transactionData in transactionDataList) {
                if (mapCategory.keys.contains(transactionData.category!!)) {
                    mapCategory[transactionData.category!!] =
                        mapCategory.get(transactionData.category)?.toDouble()
                            ?.plus(transactionData.amount?.toDouble()!!)!!
                } else {
                    mapCategory.put(
                        transactionData.category!!,
                        transactionData.amount?.toDouble()!!
                    )
                }
            }
            categoryMapForMonthFinal.put(it.key, mapCategory)
        }
        view.setCategoryData(categoryMapForMonthFinal)
    }

    private fun makeReportAllEarningAndSpending(sortedList: MutableList<TransactionData>) {
        var allEarning: Double = 0.0
        var allSpent: Double = 0.0
        sortedList.forEach {
            if (!it.amount?.startsWith("-")!!) {
                allEarning += it.amount?.toDouble()!!
            }

            if (it.amount?.startsWith("-")!!) {
                allSpent += it.amount?.toDouble()!!
            }
        }
        view.setAllSpending(allSpent)
        view.setAllEarning(allEarning)
    }

    fun setStartDate(date: String) {
        startDate = date
    }

    fun setEndDate(date: String) {
        endDate = date
    }

}