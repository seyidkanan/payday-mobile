package com.paydaybank.android.monthly_dashboard

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.paydaybank.android.R
import com.paydaybank.android.transaction.TransactionListActivity
import kotlinx.android.synthetic.main.activity_monthly_dashboard.*
import kotlinx.android.synthetic.main.empty_view.*
import kotlinx.android.synthetic.main.progress_view.*
import java.lang.StringBuilder
import java.util.*

class MonthlyDashboardActivity : AppCompatActivity(), MonthlyDashboardView {

    private lateinit var presenter: MonthlyDashboardPresenter
    private lateinit var linearCategoryData: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_dashboard)

        linearCategoryData = findViewById(R.id.linearLayoutCategory)
        presenter = MonthlyDashboardPresenter(this)

        presenter.makeMonthlyReport()
    }

    fun onClickStartDateButton(view: View) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this,
            { _, year, monthOfYear, dayOfMonth ->
                val localMonth = monthOfYear + 1
                val date: String
                val localDay: String
                if (dayOfMonth.toString().length == 1) {
                    localDay = "0$dayOfMonth"
                } else {
                    localDay = "$dayOfMonth"
                }
                date = if (localMonth.toString().length == 1) {
                    "$year-0$localMonth-$localDay"
                } else {
                    "$year-$localMonth-$localDay"
                }
                presenter.setStartDate(date)
                btnStartDate.text = date
                presenter.makeMonthlyReport()
            },
            year,
            month,
            day
        )
        datePickerDialog.show()
    }

    fun onClickEndDateButton(view: View) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this,
            { _, year, monthOfYear, dayOfMonth ->
                val localMonth = monthOfYear + 1
                val date: String
                val localDay: String
                if (dayOfMonth.toString().length == 1) {
                    localDay = "0$dayOfMonth"
                } else {
                    localDay = "$dayOfMonth"
                }

                date = if (localMonth.toString().length == 1) {
                    "$year-0$localMonth-$localDay"
                } else {
                    "$year-$localMonth-$localDay"
                }
                presenter.setEndDate(date)
                btnEndDate.text = date
                presenter.makeMonthlyReport()
            },
            year,
            month,
            day
        )
        datePickerDialog.show()

    }

    override fun setAllSpending(allSpent: Double) {
        textViewAllSpending.text = "all spending $allSpent"
    }

    override fun setAllEarning(allEarning: Double) {
        textViewAllEarning.text = "all earning $allEarning"
    }

    override fun setCategoryData(categoryMap: MutableMap<String, MutableMap<String, Double>>) {
        val textView = TextView(this)
        var dataText = StringBuilder()
        categoryMap.iterator().forEach {
            dataText.append("${it.key}").append("\n")
            it.value.iterator().forEach {
                dataText.append("${it.value} ${it.key}").append("\n")
            }
        }
        textView.text = dataText
        linearCategoryData.addView(textView)

    }

    override fun clearData() {
        textViewAllEarning.text = "N/A"
        textViewAllSpending.text = "N/A"
        linearCategoryData.removeAllViews()
    }

    override fun showProgress() {
        frameLayoutProgressView.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        frameLayoutProgressView.visibility = View.GONE
    }

    override fun showErrorMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showDataView() {
        linearLayoutData.visibility = View.VISIBLE
    }

    override fun hideDataView() {
        linearLayoutData.visibility = View.GONE
    }

    override fun showEmptyView() {
        linearLayoutEmptyView.visibility = View.VISIBLE
    }

    override fun hideEmptyView() {
        linearLayoutEmptyView.visibility = View.GONE
    }

    fun onClickTransaction(view: View) {
        startActivity(Intent(this, TransactionListActivity::class.java))
    }

}