package com.paydaybank.android.monthly_dashboard

import com.paydaybank.android.BaseView

interface MonthlyDashboardView : BaseView {

    fun setAllSpending(allSpent: Double)

    fun setAllEarning(allEarning: Double)

    fun setCategoryData(categoryMap: MutableMap<String, MutableMap<String, Double>>)

    fun clearData()

}