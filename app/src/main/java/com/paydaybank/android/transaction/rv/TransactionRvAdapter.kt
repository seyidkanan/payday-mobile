package com.paydaybank.android.transaction.rv

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.paydaybank.android.R
import com.paydaybank.android.pojo.TransactionData
import java.text.SimpleDateFormat
import java.util.*


class TransactionRvAdapter : RecyclerView.Adapter<TransactionViewHolder>() {

    private var transactionList: MutableList<TransactionData> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TransactionViewHolder(inflater = inflater, parent = parent)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(getData(position))
    }

    override fun getItemCount(): Int {
        return transactionList.size
    }

    fun addDataAndRefreshView(listData: MutableList<TransactionData>) {
        transactionList.clear()
        transactionList.addAll(listData)
        notifyDataSetChanged()
    }

    private fun getData(pos: Int): TransactionData {
        return transactionList[pos]
    }

}

class TransactionViewHolder(inflater: LayoutInflater, parent: ViewGroup) : RecyclerView.ViewHolder(
    inflater.inflate(
        R.layout.item_view_transaction, parent, false
    )
) {

    private var textViewAmount: TextView = itemView.findViewById(R.id.textViewAmount)
    private var textViewCategory: TextView = itemView.findViewById(R.id.textViewCategory)
    private var textViewShortDesc: TextView = itemView.findViewById(R.id.textViewShortDesc)
    private var textViewDate: TextView = itemView.findViewById(R.id.textViewDate)

    fun bind(data: TransactionData) {
        if (data.amount != null && data.amount!!.isNotEmpty()) {
            textViewAmount.text = data.amount
        } else {
            textViewAmount.text = "N/A"
        }
        if (data.vendor != null && data.vendor!!.isNotEmpty()) {
            textViewShortDesc.text = data.vendor
        } else {
            textViewShortDesc.text = "N/A"
        }
        if (data.category != null && data.category!!.isNotEmpty()) {
            textViewCategory.text = data.category
        } else {
            textViewCategory.text = "N/A"
        }
        if (data.date != null && data.date!!.isNotEmpty()) {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val outputFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
            val date: Date = inputFormat.parse(data.date)
            val formattedDate: String = outputFormat.format(date)
            textViewDate.text = formattedDate
        } else {
            textViewDate.text = "N/A"
        }
    }

}