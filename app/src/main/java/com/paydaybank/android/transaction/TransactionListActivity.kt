package com.paydaybank.android.transaction

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.paydaybank.android.R
import com.paydaybank.android.pojo.TransactionData
import com.paydaybank.android.transaction.rv.TransactionRvAdapter
import kotlinx.android.synthetic.main.empty_view.*
import kotlinx.android.synthetic.main.progress_view.*

class TransactionListActivity : AppCompatActivity(), TransactionListView {

    private lateinit var presenter: TransactionListPresenter

    private lateinit var rvAdapter: TransactionRvAdapter
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)
        presenter = TransactionListPresenter(this)

        initRV()

        presenter.fetchAllTransaction()
    }

    private fun initRV() {
        rvAdapter = TransactionRvAdapter()
        recyclerView = findViewById<RecyclerView>(R.id.transactionRecycleView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = rvAdapter
    }

    override fun showData(list: MutableList<TransactionData>) {
        rvAdapter.addDataAndRefreshView(list)
    }

    override fun showProgress() {
        frameLayoutProgressView.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        frameLayoutProgressView.visibility = View.GONE
    }

    override fun showErrorMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun showEmptyView() {
        linearLayoutEmptyView.visibility = View.VISIBLE
    }

    override fun hideEmptyView() {
        linearLayoutEmptyView.visibility = View.GONE
    }

    override fun showDataView() {
        recyclerView.visibility = View.VISIBLE
    }

    override fun hideDataView() {
        recyclerView.visibility = View.GONE
    }

}