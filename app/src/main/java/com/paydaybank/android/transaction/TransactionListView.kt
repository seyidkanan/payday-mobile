package com.paydaybank.android.transaction

import com.paydaybank.android.BaseView
import com.paydaybank.android.pojo.TransactionData

interface TransactionListView : BaseView {

    fun showData(list: MutableList<TransactionData>)


}