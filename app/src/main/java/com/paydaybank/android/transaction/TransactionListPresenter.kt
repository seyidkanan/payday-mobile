package com.paydaybank.android.transaction

import com.paydaybank.android.api.TransactionInteractor
import com.paydaybank.android.pojo.TransactionData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.Comparator


class TransactionListPresenter(private var view: TransactionListView) {

    private var transactionInteractor: TransactionInteractor = TransactionInteractor()

    constructor(
        view: TransactionListView,
        transactionInteractor: TransactionInteractor
    ) : this(view) {
        this.transactionInteractor = transactionInteractor
        this.view = view
    }

    fun fetchAllTransaction() {
        view.showProgress()
        transactionInteractor.fetchTransaction(accountId = 1,
            object : Callback<MutableList<TransactionData>> {
                override fun onResponse(
                    call: Call<MutableList<TransactionData>>,
                    response: Response<MutableList<TransactionData>>
                ) {
                    view.hideProgress()
                    if (response.isSuccessful) {
                        response.body()?.let {
                            if (it.size > 0) {
                                sortDate(it)
                                view.showDataView()
                                view.hideEmptyView()
                            } else {
                                view.showEmptyView()
                                view.hideDataView()
                            }
                        }
                    } else {
                        view.showEmptyView()
                        view.hideDataView()
                    }
                }

                override fun onFailure(call: Call<MutableList<TransactionData>>, t: Throwable) {
                    view.hideProgress()
                    t.message?.let { view.showErrorMessage(it) }
                    view.showEmptyView()
                    view.hideDataView()
                }

            })
    }

    private fun sortDate(list: MutableList<TransactionData>) {
        list.sortWith(Comparator { p0, p1 -> p0?.date?.let { p1?.date?.compareTo(it) }!! })
        view.showData(list)
    }

}