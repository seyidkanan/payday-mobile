package com.paydaybank.android.pojo

import com.google.gson.annotations.SerializedName

data class TransactionData(
    var id: Int? = null,
    @SerializedName("account_id")
    var accountId: Int? = null,
    var amount: String? = null,
    var vendor: String? = null,
    var category: String? = null,
    var date: String? = null
)