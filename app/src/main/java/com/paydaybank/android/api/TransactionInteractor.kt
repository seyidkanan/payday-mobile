package com.paydaybank.android.api

import com.paydaybank.android.pojo.TransactionData
import retrofit2.Callback


class TransactionInteractor {

    private val apiRequest = ApiServiceBuilder.buildService(Endpoints::class.java)

    fun fetchTransaction(accountId: Int, callback: Callback<MutableList<TransactionData>>) {
        apiRequest.fetchTransaction(accountId = accountId)
            .enqueue(callback)
    }

}