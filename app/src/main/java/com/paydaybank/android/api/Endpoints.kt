package com.paydaybank.android.api

import com.paydaybank.android.pojo.TransactionData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Endpoints {

    @GET("transactions")
    fun fetchTransaction(@Query("account_id") accountId: Int): Call<MutableList<TransactionData>>

}