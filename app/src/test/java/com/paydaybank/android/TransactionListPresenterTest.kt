package com.paydaybank.android

import com.nhaarman.mockito_kotlin.capture
import com.nhaarman.mockito_kotlin.mock
import com.paydaybank.android.api.TransactionInteractor
import com.paydaybank.android.pojo.TransactionData
import com.paydaybank.android.transaction.TransactionListPresenter
import com.paydaybank.android.transaction.TransactionListView
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class TransactionListPresenterTest {

    private lateinit var presenter: TransactionListPresenter

    @JvmField
    @Rule
    var mockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var view: TransactionListView

    @Mock
    private lateinit var interactor: TransactionInteractor

    @Captor
    private lateinit var captor: ArgumentCaptor<Callback<MutableList<TransactionData>>>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this);
        presenter = TransactionListPresenter(view, interactor)
    }

    @Test
    fun testFetchTransactionResultSuccessWithData() {
        val mockCall: Call<MutableList<TransactionData>> = mock()

        presenter.fetchAllTransaction()

        verify(interactor).fetchTransaction(anyInt(), capture(captor))

        val callback = captor.value

        var transactionList = mutableListOf<TransactionData>()
        transactionList.add(
            TransactionData(
                1,
                2,
                "",
                "",
                "",
                ""
            )
        )

        callback.onResponse(mockCall, Response.success(200, transactionList))

        val inOrder = inOrder(view)
        inOrder.verify(view).hideProgress()
        inOrder.verify(view).showDataView()
        inOrder.verify(view).hideEmptyView()
    }

    @Test
    fun testFetchTransactionResultSuccessWithNoData() {
        val mockCall: Call<MutableList<TransactionData>> = mock()

        presenter.fetchAllTransaction()

        verify(interactor).fetchTransaction(anyInt(), capture(captor))

        val callback = captor.value

        var transactionList = mutableListOf<TransactionData>()

        callback.onResponse(mockCall, Response.success(200, transactionList))

        val inOrder = inOrder(view)
        inOrder.verify(view).hideProgress()
        inOrder.verify(view).showEmptyView()
        inOrder.verify(view).hideDataView()
    }

    @Test
    fun testFetchTransactionsResultFail() {
        val mockCall: Call<MutableList<TransactionData>> = mock()
        val mockThrowable: Throwable = mock()

        presenter.fetchAllTransaction()

        verify(interactor).fetchTransaction(anyInt(), capture(captor))

        val callback = captor.value

        callback.onFailure(mockCall, mockThrowable)

        val inOrder = inOrder(view)
        inOrder.verify(view).hideProgress()
        inOrder.verify(view).showEmptyView()
        inOrder.verify(view).hideDataView()
    }

}