package com.paydaybank.android

import com.nhaarman.mockito_kotlin.capture
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.paydaybank.android.api.TransactionInteractor
import com.paydaybank.android.monthly_dashboard.MonthlyDashboardPresenter
import com.paydaybank.android.monthly_dashboard.MonthlyDashboardView
import com.paydaybank.android.pojo.TransactionData
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.*
import org.mockito.junit.MockitoJUnit
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MonthlyDashboardPresenterTest {

    @JvmField
    @Rule
    var mockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var interactor: TransactionInteractor

    @Mock
    private lateinit var view: MonthlyDashboardView

    @Captor
    private lateinit var captor: ArgumentCaptor<Callback<MutableList<TransactionData>>>

    private lateinit var presenter: MonthlyDashboardPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this);
        presenter = MonthlyDashboardPresenter(view, interactor)
    }

    @Test
    fun testMakeMonthlyReportFetchTransactionResultSuccessWithData() {
        val mockCall: Call<MutableList<TransactionData>> = mock()

        presenter.setStartDate("2019-03-01")
        presenter.setEndDate("2019-03-30")

        presenter.makeMonthlyReport()


        Mockito.verify(interactor).fetchTransaction(Mockito.anyInt(), capture(captor))

        val callback = captor.value

        var transactionList = mutableListOf<TransactionData>()
        transactionList.add(
            TransactionData(
                2,
                1,
                "7196.35",
                "Grimes, Quigley and Runolfsson",
                "Salary",
                "2019-03-17T00:04:00Z"
            )
        )
        transactionList.add(
            TransactionData(
                13,
                1,
                "-240.96",
                "Price-Stroman",
                "Jewelery",
                "2019-03-27T01:12:58Z"
            )
        )
        transactionList.add(
            TransactionData(
                17,
                1,
                "-14.82",
                "Price-Stroman",
                "Jewelery",
                "2019-03-24T05:11:47Z"
            )
        )

        callback.onResponse(mockCall, Response.success(200, transactionList))

        val inOrder = Mockito.inOrder(view)
        inOrder.verify(view).clearData()
        inOrder.verify(view).showProgress()
        inOrder.verify(view).hideProgress()
        inOrder.verify(view).setAllSpending(Mockito.anyDouble())
        inOrder.verify(view).setAllEarning(Mockito.anyDouble())
        inOrder.verify(view).setCategoryData(Mockito.anyMap())
    }

    @Test
    fun testReportAllEarningAndSpending() {
        var transactionList = mutableListOf<TransactionData>()
        transactionList.add(
            TransactionData(
                2,
                1,
                "7196.35",
                "Grimes, Quigley and Runolfsson",
                "Salary",
                "2019-03-17T00:04:00Z"
            )
        )
        transactionList.add(
            TransactionData(
                13,
                1,
                "-240.96",
                "Price-Stroman",
                "Jewelery",
                "2019-03-27T01:12:58Z"
            )
        )
        transactionList.add(
            TransactionData(
                17,
                1,
                "-14.82",
                "Price-Stroman",
                "Jewelery",
                "2019-03-24T05:11:47Z"
            )
        )

        val startDate = "2019-03-01"
        val endDate = "2019-03-30"
        presenter.setStartDate(startDate)
        presenter.setEndDate(endDate)
        presenter.makeReport(
            startDate = startDate,
            endDate = endDate,
            transactionList = transactionList
        )

        verify(view).setAllEarning(7196.35)
        verify(view).setAllSpending(-255.78)
    }

    @Test
    fun testMakeMonthlyReportFetchTransactionResultSuccessWithNoData() {
        val mockCall: Call<MutableList<TransactionData>> = mock()

        presenter.setStartDate("2019-03-01")
        presenter.setEndDate("2019-03-30")

        presenter.makeMonthlyReport()

        Mockito.verify(interactor).fetchTransaction(Mockito.anyInt(), capture(captor))

        val callback = captor.value

        var transactionList = mutableListOf<TransactionData>()

        callback.onResponse(mockCall, Response.success(200, transactionList))

        val inOrder = Mockito.inOrder(view)
        inOrder.verify(view).clearData()
        inOrder.verify(view).showProgress()
        inOrder.verify(view).hideProgress()
        inOrder.verify(view).showEmptyView()
        inOrder.verify(view).hideDataView()
    }

    @Test
    fun testMakeMonthlyReportFetchTransactionResultFail() {
        val mockCall: Call<MutableList<TransactionData>> = mock()

        presenter.setStartDate("2019-03-01")
        presenter.setEndDate("2019-03-30")

        presenter.makeMonthlyReport()

        Mockito.verify(interactor).fetchTransaction(Mockito.anyInt(), capture(captor))

        val callback = captor.value
        callback.onFailure(mockCall, Throwable("mock test"))

        val inOrder = Mockito.inOrder(view)
        inOrder.verify(view).clearData()
        inOrder.verify(view).showProgress()
        inOrder.verify(view).hideProgress()
        inOrder.verify(view).showErrorMessage(Mockito.anyString())
        inOrder.verify(view).showEmptyView()
        inOrder.verify(view).hideDataView()
    }

}